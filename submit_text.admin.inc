<?php

/**
* @file
* Submit Text admin file.
*/

/**
* Implements hook_form().
*/
function submit_text_admin_form($form, &$form_state) {

// Get available content types.
$node_types = node_type_get_types();

// Variable to display 1st fieldset collapse open.
$i = 0;
// Generate fieldset for each content type along with exclude, min,
// max and unique form elements.
foreach ($node_types as $type) {

// Display First fieldset collapsed open.
    if ($i == 0) {
        $form[$type->type] = array(
            '#type' => 'fieldset',
            '#title' => check_plain($type->name),
            '#collapsible' => TRUE,
            '#collapsed' => FALSE,
        );
    } else {
        $form[$type->type] = array(
            '#type' => 'fieldset',
            '#title' => check_plain($type->name),
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
        );
    }
// Increment $i for other fieldsets in collapsed closed.
    $i++;

    $form[$type->type]['submit-title-' . $type->type] = array(
        '#type' => 'textfield',
        '#title' => t("Submit text"),
        '#description' => t("please provide label for submit button"),
        '#size' => 50,
        '#default_value' => isset(variable_get('submit_text_config')[$type->type]) ? variable_get('submit_text_config')[$type->type] : '',
    );
}
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Set Title',
    );
    return $form;
}

/**
 * Implements hook_form_submit().
 */
function submit_text_admin_form_submit($form, &$form_state) {

    $values = array();
    // Get available content types.
    $node_types = node_type_get_types();

    // Store Form values in node_title_validation_config variable.
    foreach ($node_types as $type) {
        $values[$type->type]['submit-title'] = $form_state['values']['submit-title-' . $type->type];
    }
    // Set submit_text_config variable.
    variable_set('submit_text_config', $values);

    drupal_set_message(t('Configuration saved successfully!'));
}