INTRODUCTION
-------------
A light module, helps admin to set the submit button text only
on node creation page.for more information visit module description page

INSTALLATION
------------
 * Install contributed Drupal module as we do normally. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * Configure user permissions in Administration » People » Permissions:
    submit text admin form

 * Configure Node Title Validation in Administration » Configuration »
    Content authoring » Submit Text configuration

    admin/config/content/admin/config/content/submit_text


MAINTAINERS
-----------
Current maintainers:
 * jaywant topno (xaiwant) - https://www.drupal.org/u/xaiwant
 * Karthik Kumar D K (heykarthikwithu) - https://www.drupal.org/u/heykarthikwithu
